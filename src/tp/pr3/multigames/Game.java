/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.multigames;

import tp.pr3.*;
import tp.pr3.control.commands.GameOverException;

import java.util.Random;

public class Game {
	private Board board;
	private int size, initCells, score, bestValue;
	private Random random;
	private GameStateStack undoStack = new GameStateStack();
	private GameStateStack redoStack = new GameStateStack();
	private GameRules currentRules;
	private String rulesType;

	public Game(int size, int initCells, long seed, GameRules rules, String type) {
		this.size = size;
		this.initCells = initCells;
		this.random = new Random(seed);
		currentRules = rules;
		rulesType = type;
		board = currentRules.createBoard(size);
		currentRules.initBoard(board, initCells, random);
		bestValue = currentRules.getWinValue(board);
	}

	public void move(Direction dir){
		undoStack.push(getState());
		if (!redoStack.isEmpty()){
			redoStack = new GameStateStack();
		}

		MoveResults m = board.executeMove(dir, currentRules);
		if (m.isMoved()){
			score += m.getPoints();
			currentRules.addNewCell(board, random);
			bestValue = currentRules.getWinValue(board);
		}
	}

	public void reset(){
		board = currentRules.createBoard(size);
		currentRules.initBoard(board, initCells, random);
		score = 0;
		bestValue = currentRules.getWinValue(board);
		undoStack = new GameStateStack();
		redoStack = new GameStateStack();
	}

	/**
	 * push(estado actual) en la pila de redo
	 * estado actual = pop() de la pila de undo
	 * @return undone successfully
	 */
	public boolean undo(){
		GameState aux = getState();
		setState(undoStack.pop());
		redoStack.push(aux);
		return true;
	}

	/**
	 * push(estado actual) en la pila de undo
	 * estado actual = pop() de la pila de redo
	 * @return redone successfully
	 */
	public boolean redo(){
		GameState aux = getState();
		setState(redoStack.pop());
		undoStack.push(aux);
		return true;
	}

	public int getSize() {
		return size;
	}

	public String store(){
		String toPrint = "This file stores a saved 2048 game\n\n";
		toPrint += board.store();
		toPrint += "\n" + score + "\t" + bestValue + "\t" + rulesType;
		return toPrint;
	}

	private GameState getState(){
		GameState gs = new GameState();
		gs.setBoardState(board.getState());
		gs.setHighest(bestValue);
		gs.setScore(score);
		return  gs;
	}

	private void setState(GameState aState){
		board.setState(aState.getBoardState());
		bestValue= aState.getHighest();
		score = aState.getScore();
	}

	public void load(int size, GameRules rules, String type, int score, int	bestValue, String stringBoard){
		//modify current game with new params and type
		this.size = size;
		currentRules = rules;
		rulesType = type;
		board = currentRules.createBoard(size);
		this.score = score;
		this.bestValue = bestValue;
		undoStack = new GameStateStack();
		redoStack = new GameStateStack();
		// array of values
		String[] boardArray = stringBoard.trim().split("\t");
		//Fill Board from boardArray
		int cont = 0;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (!boardArray[cont].equals("0")){
					board.setCell(new Position(i,j),Integer.parseInt(boardArray[cont]));
				}
				cont++;
			}
		}
	}

	@Override
	public String toString() {
		return board.toString() +"\nScore: " + score + " Best value: " + bestValue;
	}

	public void isEnded() throws GameOverException {
		if (currentRules.win(board)) {
			throw new GameOverException("Congratulations, you won :)");
		} else if (currentRules.lose(board)){
			throw new GameOverException("Nice try, you lost :(");
		}
	}

	public void gameNew(int newSize, int newCells, long newSeed, GameRules newRules, String type){
		size = newSize;
		initCells = newCells;
		random = new Random(newSeed);
		currentRules = newRules;
		rulesType = type;
		board = currentRules.createBoard(size);
		currentRules.initBoard(board, initCells, random);
		bestValue = currentRules.getWinValue(board);
	}
}
