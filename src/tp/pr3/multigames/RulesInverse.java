/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.multigames;

import tp.pr3.Board;
import tp.pr3.Cell;
import tp.pr3.Position;
import java.util.Random;

public class RulesInverse implements GameRules {
	private final int winValue = 1;

	public void addNewCellAt(Board board, Position pos, Random rand) {
		float rdn = rand.nextFloat();
		int value = 2048;

		if (rdn > 0.9) {
			value = 1024;
		}

		board.setCell(pos, value);
	}

	public int getWinValue(Board board) {
		return board.getMin();
	}

	public boolean win(Board board) {
		return getWinValue(board) == winValue;
	}

	public int merge(Cell self, Cell other) {
		int value = self.getValue() / 2 ;

		self.setValue(value);
		other.setValue(0);

		return 2048 / value;
	}
}
