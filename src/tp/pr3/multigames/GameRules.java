/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.multigames;

import tp.pr3.Board;
import tp.pr3.Cell;
import tp.pr3.Position;
import java.util.Random;

public interface GameRules {
	void addNewCellAt(Board board, Position pos, Random rand);
	int getWinValue(Board board);
	boolean win(Board board);

	default int merge(Cell self, Cell other) {
		int value = self.getValue() + other.getValue();

		self.setValue(value);
		other.setValue(0);

		return value;
	}

	default boolean canMerge(Cell self, Cell other) {
		return self.getValue() == other.getValue();
	}

	default boolean lose(Board board) {
		return board.isFull() && !board.canMerge(this);
	}

	default Board createBoard(int size) {
		return new Board(size);
	}

	default void addNewCell(Board board, Random rand) {
		addNewCellAt(board, board.freePosition(rand), rand);
	}

	default void initBoard(Board board, int numCells, Random rand) {
		for (int i = 0; i < numCells; i++) {
			addNewCell(board, rand);
		}
	}
}
