/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3;

import tp.pr3.multigames.GameRules;
import tp.pr3.multigames.Rules2048;
import tp.pr3.multigames.RulesFib;
import tp.pr3.multigames.RulesInverse;

public enum GameType {
	ORIG("2048, original version", "original", new Rules2048()),
	FIB("2048, Fibonacci version", "fib", new RulesFib()),
	INV("2048, inverse version", "inverse", new RulesInverse());


	private String userFriendlyName;
	private String parameterName;
	private GameRules correspondingRules;

	private GameType(String friendly, String param, GameRules rules){
		userFriendlyName = friendly;
		parameterName = param;
		correspondingRules = rules;
	}

	// precondition : param string contains only lower−case characters
	// used in PlayCommand and Game, in parse method and load method, respectively
	public static GameType parse(String param) {
		for (GameType gameType : GameType.values()) {
			if (gameType.parameterName.equals(param))
				return gameType;
		}
		return null;
	}

	// used in PlayCommand to build help message, and in parse method exception msg
	public static String externaliseAll () {
		String s = "";
		for (GameType type : GameType.values())
			s = s + " " + type.parameterName + ",";
		return s.substring(1, s. length() - 1);
	}

	// used in Game when constructing object and when executing play command
	public GameRules getRules() {
		return correspondingRules;
	}

	// used in Game in store method
	public String externalise () {
		return parameterName;
	}

	// used PlayCommand and LoadCommand, in parse methods
	// in ack message and success message, respectively
	public String toString() {
		return userFriendlyName;
	}
}