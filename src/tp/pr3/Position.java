/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3;

public class Position {
	private int row, column;

	public Position(int row, int column) {
		this.row = row;
		this.column = column;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public Position neighbour(Position pos, Direction dir) {
		switch(dir){
			case DOWN:
				pos.setRow(pos.getRow() - 1);
				break;
			case LEFT:
				pos.setColumn(pos.getColumn() - 1);
				break;
			case RIGHT:
				pos.setColumn(pos.getColumn() + 1);
				break;
			case UP:
				pos.setRow(pos.getRow() + 1);
				break;
			default:
				break;
		}

		return pos;
	}
}
