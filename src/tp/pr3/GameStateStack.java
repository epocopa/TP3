/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3;

import java.util.EmptyStackException;

public class GameStateStack {
	private static final int CAPACITY = 21;
	private GameState[] buffer = new GameState[CAPACITY];
	private int start, nextPos, blank = CAPACITY - 1;
	/**
	 * @return Devuelve el último estado almacenado
	 */
	public GameState pop(){
		if (!isEmpty()) {
			nextPos = previous(nextPos);
			return buffer[nextPos];
		} else { throw new EmptyStackException(); }
	}

	/**
	 * @return Almacena un nuevo estado
	 */
	public void push(GameState state){
		if (isFull()){
			blank = next(blank);
			start = next(start);
		}
		buffer[nextPos] = state;
		nextPos = next(nextPos);
	}

	/**
	 * @return Devuelve true si la secuencia está vacía
	 */
	public boolean isEmpty() {
		return start == nextPos;
	}

	private boolean isFull(){
		 return nextPos == blank;
	}

	private int previous(int pos){
		if (pos == 0){
			return CAPACITY-1;
		} else {
			return --pos;
		}
	}

	private int next(int pos){
		if (pos == CAPACITY-1){
			return 0;
		} else {
			return ++pos;
		}
	}
}
