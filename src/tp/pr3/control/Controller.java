/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.control;

import tp.pr3.control.commands.Command;
import tp.pr3.control.commands.CommandUsageException;
import tp.pr3.control.commands.GameOverException;
import tp.pr3.multigames.*;
import tp.pr3.util.CommandParser;

import java.util.EmptyStackException;
import java.util.Scanner;

public class Controller {
	private Game game;
	private Scanner in;

	public Controller(Game game, Scanner in) {
		this.game = game;
		this.in = in;
	}

	public void run() {
		String[] commandWords;
		Command command;
		System.out.println(game.toString());
		try {
			do {
				System.out.print("~$ ");
				commandWords = in.nextLine().toLowerCase().trim().split(" ");
				try {
					command = CommandParser.parseCommand(commandWords, in);
					if (command != null) {
						if (command.execute(game)) {
							System.out.println(game.toString());
						}
					} else {
						System.out.println("Unknown command");
					}
				} catch (CommandUsageException e) {
					System.err.println(e.getMessage());
				} catch (EmptyStackException e) {
					System.err.println("Nothing to " + commandWords[0]);
				}
				game.isEnded();
			} while (true);
		} catch (GameOverException e) {
			System.err.println(e.getMessage());
		}
	}
}