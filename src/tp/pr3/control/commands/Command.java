/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.control.commands;

import tp.pr3.multigames.Game;

import java.util.Scanner;

public abstract class Command {
	private String helpText;
	private String commandText;
	protected final String commandName;
	
	public Command(String commandInfo, String helpInfo){
		commandText = commandInfo;
		helpText = helpInfo;
		String[] commandInfoWords = commandText.split("\\s+");
		commandName = commandInfoWords[0];
	}

	public Command() {
		commandName = null;
	}

	// execute returns a boolean to tell the controller whether or not to print the game
	public abstract boolean execute(Game game);
	public abstract Command parse(String[] commandWords, Scanner in) throws CommandUsageException;
	public String helpText(){
		return "\t" + commandText + " -> " + helpText;
	}
	public String commandText(){
		return commandText;
	}
	
}
