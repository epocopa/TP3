/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.control.commands;

import tp.pr3.GameType;
import tp.pr3.multigames.Game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class LoadCommand extends Command{
	private String filename;
	private static String commandText = "load";
	private static String helpText = "Load game from a file";

	public LoadCommand(String filename){
		super(commandText, helpText);
		this.filename = filename;
	}

	public LoadCommand(){
		super(commandText, helpText);
	}

	@Override
	public boolean execute(Game game) {
		String board = "", line, type;
		String[] lastLine;
		int size = 0;

		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			if (!(br.readLine()).equals("This file stores a saved 2048 game")) {
				return false;
				//TODO: exception
			}
			br.readLine();
			while (!(line = br.readLine()).equals("")) {
				size = line.trim().split("\t").length;
				board += line;
			}
			lastLine = br.readLine().trim().split("\t");
			type = lastLine[2] ;
			game.load(size, GameType.parse(type).getRules(), type,
					Integer.parseInt(lastLine[0]), Integer.parseInt(lastLine[1]), board);
			System.out.println("Game successfully loaded from file: " + filename + ", " + type +  " version");
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Command parse(String[] commandWords, Scanner in) throws CommandUsageException {
		//TODO: exceptions
		Command command = null;
		if (commandWords[0].equals(commandText)) {
			if (commandWords.length == 2) {
				if (new File(commandWords[1]).exists()) {
					command = new LoadCommand(commandWords[1]);
				} else {
					System.out.println("File not found");
				}
			} else {
				throw new CommandUsageException("Usage: " + commandText + " <filename>");
			}
		}
		return command;
	}
}
