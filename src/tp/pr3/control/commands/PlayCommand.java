/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.control.commands;

import tp.pr3.GameType;
import tp.pr3.multigames.Game;
import tp.pr3.multigames.GameRules;

import java.util.Random;
import java.util.Scanner;

public class PlayCommand extends Command {
	private GameType type;
	private static String commandText = "play";
	private static String helpText = "Starts the desired game variant";
	private int newSize = 4, newCells = 2,newSeed;
	private GameRules newRules;

	public PlayCommand(GameType type, int newSize, int newCells, int newSeed, GameRules newRules) {
		super(commandText, helpText);
		this.type = type;
		this.newSize = newSize;
		this.newCells = newCells;
		this.newSeed = newSeed;
		this.newRules = newRules;
	}

	public PlayCommand() {
		super(commandText, helpText);
	}

	@Override
	public boolean execute(Game game) {
		game.gameNew(newSize, newCells, newSeed, newRules, type.toString());

		return true;
	}

	@Override
	public Command parse(String[] commandWords, Scanner in) throws CommandUsageException {
		String aux;

		Command command = null;

		if (commandWords[0].equals(commandText)) {
			if (commandWords.length == 2) {
				try {
					GameType type = GameType.valueOf(commandWords[1].toUpperCase());

					System.out.print("If the parameters are not provided or can't be used, the default parameters will be used.\n");
					System.out.print("Provide the new size: ");
					aux = in.nextLine();
					if (!aux.isEmpty()) {
						newSize = Integer.valueOf(aux);
					} else {
						System.out.println("\tUsing the default size of the board: 4");
					}


					System.out.print("Provide the number of initial cells: ");
					aux = in.nextLine();
					if (!aux.isEmpty()) {
						newCells = Integer.valueOf(aux);
					} else {
						System.out.println("\tUsing the default number of initial cells: 2");
					}


					System.out.print("Provide the new game seed: ");
					aux = in.nextLine();
					if (!aux.isEmpty()) {
						newSeed = Integer.valueOf(aux);
					} else {
						newSeed = new Random().nextInt(1000);
						System.out.println("\tUsing random seed: " + newSeed);
					}
					newRules = type.getRules();

					command = new PlayCommand(type, newSize, newCells, newSeed, newRules);
				} catch (IllegalArgumentException e) {
					throw new CommandUsageException("The gametype of play must be: Orig, Fib, Inv");
				}
			} else {
				throw new CommandUsageException("Usage: " + commandText + " <gametype>");
			}
		}
		return command;
	}
}
