/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.control.commands;

import tp.pr3.multigames.Game;
import tp.pr3.util.MyStringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SaveCommand extends Command{
	private String filename;
	private static String commandText = "save";
	private static String helpText = "Save game in a file";

	public SaveCommand(String filename){
		super(commandText, helpText);
		this.filename = filename;
	}

	public SaveCommand(){
		super(commandText, helpText);
	}


	@Override
	public boolean execute(Game game) {
		try (BufferedWriter br = new BufferedWriter(new FileWriter(filename))){
			String toPrint = game.store();
			br.write(toPrint);
			br.newLine();
			System.out.println("Game successfully saved to file; use load command to reload it.");
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Command parse(String[] commandWords, Scanner in) throws CommandUsageException {
		//TODO: exceptions
		Command command = null;
		boolean overwrite = false;

		if (commandWords[0].equals(commandText)){
			if (commandWords.length == 2) {
				String name = commandWords[1];
				if (MyStringUtils.validFileName(name)) {
					while (new File(name).exists() && !overwrite) {
						System.out.println("Overwrite? (Y/N)");
						if (in.nextLine().toLowerCase().equals("y")) {
							overwrite = true;
						} else {
							System.out.print("New filename: ");
							name = in.nextLine();
						}
					}
					command = new SaveCommand(name);
				} else {
					throw new CommandUsageException("Invalid filename");
				}
			} else {
				throw new CommandUsageException("Usage: " + commandText + " <filename>");
			}
		}
		return command;
	}
}
