/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr3.control.commands;

import tp.pr3.Direction;
import tp.pr3.multigames.Game;

import java.util.Scanner;

public class MoveCommand extends Command {
	private Direction direction;
	private static String commandText = "move";
	private static String helpText = "Executes a move in the desired direction";



	public MoveCommand(Direction direction) {
		super(commandText, helpText);
		this.direction = direction;
	}

	public MoveCommand() {
		super(commandText, helpText);
	}

	@Override
	public boolean execute(Game game) {
		game.move(direction);
		return true;
	}

	@Override
	public Command parse(String[] commandWords, Scanner in) throws CommandUsageException {
		Command command = null;
		if (commandWords[0].equals(commandText)) {
			if (commandWords.length == 2) {
				try {
					Direction dir = Direction.valueOf(commandWords[1].toUpperCase());
					command = new MoveCommand(dir);
				} catch (IllegalArgumentException e) {
					throw new CommandUsageException("The direction of move must be: Up, Down, Right, Left");
				}
			} else {
				throw new CommandUsageException("Usage: " + commandText + " <direction>");
			}
		}
		return command;
	}

}
